const members = [
    {
        id: 1,
        name: 'Axel Maurer',
        src: 'axel_maurer.png',
        function: 'Président',
        isComite: true
    },
    {
        id: 2,
        name: 'Mathieu Albiez',
        src: 'mathieu_albiez.png',
        function: 'Vice président',
        isComite: true
    },
    {
        id: 3,
        name: 'Oscar Dubuis',
        src: 'oscar_dubuis.png',
        function: 'Caissier',
        isComite: true
    },
    {
        id: 4,
        name: 'Orlane Jaquier',
        src: 'orlane_jaquier.png',
        function: '',
        isComite: false
    },
    {
        id: 5,
        name: 'Alexandre Rista',
        src: 'alexandre_rista.png',
        function: '5ème membre',
        isComite: true
    },
    {
        id: 6,
        name: 'Maxime Durand',
        src: 'maxime_durand.png',
        function: '',
        isComite: false
    },
    {
        id: 7,
        name: 'Ulysse Durand',
        src: 'ulysse_durand.png',
        function: '',
        isComite: false
    },
    {
        id: 10,
        name: 'Gergoire Rista',
        src: 'gregoire_rista.png',
        function: '',
        isComite: false
    },
    {
        id: 11,
        name: 'Camille Amstutz',
        src: 'No-personne.png',
        function: 'Secrétaire',
        isComite: true
    },
    {
        id: 12,
        name: 'Pedro Bispo Alves',
        src: 'pedro_bispo_alves.png',
        function: '',
        isComite: false
    },
    {
        id: 13,
        name: 'Olivier Mettaz',
        src: 'No-personne.png',
        function: '',
        isComite: false
    }
]

function getMembers () {
    return members
}

export default {
    getMembers:getMembers
}